﻿#include <iostream>
#include <ctime>
using namespace std;

void boardunit(char board[10][10] ,int &I , int &J, int level, bool &die, char boardR[10][10])
{
	for (int i=0;i<10;i++)
	{
		for (int j=0;j<10;j++)
		{
			board[i][j] = ' ';
			boardR[i][j] = ' ';
		}
	}
	// start board with F and E
	int RandI;
	int RandJ;
	board[0][0]='F';
	board[9][9]='E';
	die=false;
	I=0;
	J=0;

	srand(time(0));

	// increse Darkrider for level up
	for(int i=0;i< (3*level) ;i++)
	{
		do
		{
			RandI = rand() % 10;
			RandJ = rand() % 10;
		} 
		while (board[RandI][RandJ] != ' ');
		
		board[RandI][RandJ] = 'R';
	}
}

// printboard update
void printboard(char board[10][10])
{
	cout << "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-" << endl;
	for(int i=0;i<10;i++)
	{
		cout << " |";
		for(int j=0;j<10;j++)
		{
			cout << " "<< board[i][j] << " |";
		}
		cout << endl;
		cout << "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		cout << endl;
	}
}

bool checkwall(int I, int J, int move)
{
	if (J==0 && (move == 1 || move == 4 || move == 7))
	{
		return true;
	}
	else if (J == 9 && (move == 3 || move == 6 || move == 9))
	{
		return true;
	}
	else if (I == 0 && (move == 7 || move == 8 || move == 9))
	{
		return true;
	}
	else if (I == 9 && (move == 1 || move == 2 || move == 3))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void  input(int &move, int I, int J)
{
	
	do
	{
		cout << "Which way to move : Press (1-9) " ;
		cin >> move;
	} while (move < 1 || move > 9 || move == 5 || checkwall (I, J, move));
}


//Frodo move And check DarkRider and Frodo located in the same position
void frodo_move(char board[10][10], int move, int &I, int &J, bool &die) 
{
if(move == 1)
	{
		if (board[I+1][J-1]=='R'){board[I+1][J-1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I+1][J-1] = 'F';board[I][J] = ' ';I = I+1;J = J-1;}
	}
	else if (move == 2)
	{
		if (board[I+1][J]=='R')	{board[I+1][J] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I+1][J] = 'F';board[I][J] = ' ';I = I+1;	}
	}
	else if (move == 3)
	{	if (board[I+1][J+1]=='R'){board[I+1][J+1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I+1][J+1] = 'F';board[I][J] = ' ';I = I+1;J = J+1;}
	}
	else if (move == 4)
	{	
		if (board[I][J-1]=='R')	{board[I][J-1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I][J-1] = 'F';board[I][J] = ' ';I = I;J = J-1;}
	}
	else if (move == 6)
	{
		if (board[I][J+1]=='R')	{board[I][J+1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I][J+1] = 'F';board[I][J] = ' ';J = J+1;}
	}
	else if (move == 7)
	{
		if (board[I-1][J-1]=='R'){board[I-1][J-1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I-1][J-1] = 'F';board[I][J] = ' ';I = I-1;J = J-1;}
	}
	else if (move == 8)
	{
		if (board[I-1][J]=='R')	{board[I-1][J] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I-1][J] = 'F';board[I][J] = ' ';I = I-1;}
	}
	else if (move == 9)
	{
		if (board[I-1][J+1]=='R'){board[I-1][J+1] = 'F';board[I][J] = ' ';die = true;}
		else					{board[I-1][J+1] = 'F';board[I][J] = ' ';I = I-1;J = J+1;}
	}
}

void darkrider_move(char board[10][10], bool &die, char boardR[10][10])
{
	int RandR,I,J;
	for (int i=0;i<10;i++)
	{
		for (int j=0;j<10;j++)
		{
			if(board[i][j] == 'R')
			{
				do 
				{
					RandR = rand()%9+1;
					I = i;
					J = j;
					if		(RandR==1){	I++; J--;	}
					else if	(RandR==2){	I++;		}
					else if	(RandR==3){	I++; J++;	}
					else if	(RandR==4){	J--;		}
					else if	(RandR==6){	J++;		}
					else if	(RandR==7){	I--; J--;	}
					else if	(RandR==8){	I--;		}
					else if	(RandR==9){	I--; J++;	}
					else if (board[I][J]=='F') {	die = true; break; }

				} while (RandR == 5 || checkwall(i,j,RandR) || board[I][J] != ' ');
				
				if(boardR[i][j] == ' ')
				{
					boardR[I][J] = 'R';
					board[I][J] = 'R';
					board[i][j] = ' ';
				}  
			}
		}
	}
}

bool exit(char board[10][10])
{
	if (board[9][9] == 'F') 	{ return true; }
	else					{ return false;}
}

void clearboard(char boardR[10][10])
{
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++) { boardR[i][j] = ' '; }
	}
}

int main()
{
	cout << "---Frodo & The Lord of the Rings Game!---"<<endl;

	char board[10][10],boardR[10][10];
	int move,I,J;
	bool die;

	for (int i=0;i<10;i++)
	{
		cout << "Stage " << i+1 << endl;
		boardunit(board,I,J,i+1,die,boardR);
		printboard(board);
		while(!exit(board))
		{
			// input maze
			input(move,I,J);	
			// frodo move						
			frodo_move(board,move,I,J,die);
			// darkrider move
			darkrider_move(board,die,boardR);
			// print updated board
			printboard(board);
			// clearboard
			clearboard(boardR);

			if (die)
			{
				cout << endl << "Frodo is attacked by a Dark Rider" << endl << "GAME OVER!" << endl;
				break;
			}
		}
		if (die)
		{
			break;
		}		
	}
	if (!die)
	{
		cout << "Congratulations! You completed all stages! You Win!!!!";
	}
	return 0;

}
